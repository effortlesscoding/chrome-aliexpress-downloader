(function(window, document, $) {
  $(document).ready(function(e) {
    chrome.runtime.onMessage.addListener(handleBrowserActionClick);

    function handleBrowserActionClick(request, sender, sendResponse) {
      console.log(request);
      if(request.message === "action_snipe_aliexpress_images" ) {
        downloadVariationImages();
      }
    }

    function downloadVariationImages() {
      var images = $('.item-sku-image').find('img');
      getImageSourcesByClicking(images, 0);
    }

    function getImageSourcesByClicking(imageTags, i) {
      if (imageTags.length === 0) {
        return startDownloadingProductImages();
      }

      setTimeout(function() {
        if (i < imageTags.length) {
          clickAndDownload(imageTags[i++]); 
          getImageSourcesByClicking(imageTags, i);
        } else {
          startDownloadingProductImages();
        }
      }, 1500);
    }

    function startDownloadingProductImages() {
      setTimeout(function() {
        downloadProductImages();
      }, 1500);
    }

    function clickAndDownload(imageTriggerTag) {
      imageTriggerTag.click();
      var mainImage = $('.ui-image-viewer-thumb-frame').find('img')[0];
      console.log(mainImage.src);
      downloadFromImageSource(mainImage.src, fileNameFromSource(mainImage.src));
    }

    // Download product images

    function downloadProductImages() {
      console.log('DONE!');
      var thumbs = $('.image-thumb-list').find('img');
      thumbs.each(function(imgKey) {
        var img = thumbs[imgKey];
        downloadFromImageTag(img);
      });
    }

    function downloadFromImageTag(img) {
      var imgUrl = img.src;
      imgUrl = imgUrl.replace('_50x50.jpg', '');
      // Setup
      var parts = imgUrl.split('/');
      var dlFileName = parts[parts.length - 1];
      downloadFromImageSource(imgUrl, dlFileName);
    }

    function downloadFromImageSource(source, dlFileName) {
      var link = document.createElement('a');
      link.href = source;
      link.download = dlFileName;
      // Download
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
    
    // Helpers
    function fileNameFromSource(src) {
      var parts = src.split('/');
      return parts[parts.length - 1];
    }
  });
})(window, document, jQuery);